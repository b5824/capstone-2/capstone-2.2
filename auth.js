const jwt = require('jsonwebtoken');

const secret =
  '3c23825c1beb68cc53f778f3a02b965e26a0c35c40d3ecf55041b0cbde0ed0bcf61773605b0c61ba8570355a21a7bbdd5f9cb5b4900df8b13d7d9b910b3bcb4e';

const generateToken = (user) => {
  console.log('User');
  console.log(user);
  //What I'm going to serialise
  const data = {
    id: user._id,
    email: user.email,
    name: user.name,
    isAdmin: user.isAdmin,
  };
  console.log(`DATA`, data);

  return jwt.sign(data, secret, {});
};

const verify = (req, res, next) => {
  // console.log(req.user);
  const authHeader = req.headers.authorization;

  const token = authHeader.split(' ')[1];

  if (token === 'undefined') return res.send('Token cannot be null');

  jwt.verify(token, secret, (err, decodedToken) => {
    if (err) {
      res.send({
        auth: 'Failed',
        message: err.message,
      });
    } else {
      req.user = decodedToken;
      next();
    }
  });
};

let verifyAdmin = (req, res, next) => {
  // console.log(req);
  // console.log(`IS ADMIN ${req.user}`, req.user);
  if (req.user.isAdmin) {
    next();
  } else {
    return res.send({
      auth: 'Failed',
      message: 'Action Forbidden',
    });
  }
};

module.exports = {
  verify,
  generateToken,
  verifyAdmin,
};
