const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
  user: [
    {
      userId: String,
    },
  ],
  product: [
    {
      productId: String,
      totalAmount: Number,
    },
  ],
});

module.exports = mongoose.model('Order', orderSchema);
