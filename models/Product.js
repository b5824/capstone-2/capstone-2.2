const mongoose = require('mongoose');
//https://mongoosejs.com/docs/validation.html
const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Product name cannot be empty'],
  },
  categories: {
    type: String,
    enum: {
      values: [
        'Garden & Outdoors',
        'Toys, Children & Baby',
        'Clothes, Shoes, Jewellery & Accessories',
        'Sports & Outdoors',
        'Food & Grocery',
        'Home & Kitchen',
        'Software',
        'Computer & Accessories',
      ],
      message: '{VALUE} is not supported',
    },
  },
  description: {
    type: String,
    required: [true, 'Product name cannot be empty'],
  },
  price: {
    type: Number,
    required: [true, 'Product name cannot be empty'],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  orderPlacer: [
    {
      userId: {
        type: String,
        required: [true, 'Couse Id cannot be empty'],
      },
    },
  ],
});

module.exports = mongoose.model('Product', productSchema);
