//dependencies
const jwt = require('jsonwebtoken');
const secret =
  ' bd5a041753753d67927e77fad8767b16fdaf9f6ae872f8296f69903228932c3795a32f324987432cf14fa063400f14b2e04e5b020924c0b54d108e7e64369519';

//jwt creation
const generateToken = (user) => {
  const { _id, name, email, isAdmin } = user;
  //serialise the data
  const token = {
    _id,
    name,
    email,
    isAdmin,
  };

  return jwt.sign(token, secret, {});
};

//jwt verification
const verifyToken = (req, res, next) => {
  const bearerToken = req.headers.authorization;
  const token = bearerToken.split(' ')[1];

  jwt.verify(token, secret, (err, decodedToken) => {
    if (err) {
      res.send({
        auth: 'Failed',
        message: err.message,
      });
    } else {
      req.user = decodedToken;
      next();
    }
  });
};

//admin verification
const isUserAdmin = (req, res) => {
  if (req.isAdmin) {
  }
};

//export modules
module.exports = {
  generateToken,
  verifyToken,
};

/* NOTE: Reading List
- https://www.geeksforgeeks.org/jwt-authentication-with-node-js/
- https://www.section.io/engineering-education/how-to-build-authentication-api-with-jwt-token-in-nodejs/
- https://www.simplilearn.com/tutorials/nodejs-tutorial/jwt-authentication
- https://www.passportjs.org/concepts/authentication/middleware/
- https://www.simplilearn.com/tutorials/nodejs-tutorial/jwt-authentication
- https://www.npmjs.com/package/jsonwebtoken
*/
