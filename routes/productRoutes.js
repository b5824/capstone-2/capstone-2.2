const express = require('express');
const router = express.Router();
const auth = require('../auth');

const { verify, verifyAdmin } = auth;

const productControllers = require('../controllers/productControllers');

//TODO: Add auth to verify if user is admin
router.post(
  '/addProduct',
  verify,
  verifyAdmin,
  productControllers.createProduct
);

//TODO: Add auth to verify if user is logged in but not necessarily an admin
router.get('/', productControllers.findAllProduct);
router.get('/activeProducts', productControllers.findAllActiveProduct);

router.get('/find/:id', productControllers.findProductByID);
router.get('/find', productControllers.findProductByName);

router.put('/update/:id', verify, verifyAdmin, productControllers.editProduct);

router.get('/all', verify, verifyAdmin, productControllers.retrieveAllOrders);

router.put('/archive', verify, verifyAdmin, productControllers.archiveAProduct);

router.put(
  '/activateArchive',
  verify,
  verifyAdmin,
  productControllers.activateArchivedAProduct
);

router.delete('/delete/:id', productControllers.deleteProduct);

module.exports = router;
