//! Dependencies
const bcrypt = require('bcryptjs');

//! Modules
const User = require('../models/User');
const auth = require('../auth');
const Product = require('../models/Product');

let registerUser = (req, res) => {
  //* Destructuring req.body
  const { name, email, password } = req.body;

  User.countDocuments({ email }, (err, count) => {
    if (count === 0) {
      User.find({ email })
        .then((result) => {
          if (result.length === 0) {
            const hashPassword = bcrypt.hashSync(password, 10);
            const newUser = new User({ name, email, password: hashPassword });

            newUser
              .save()
              .then((result) => res.send(result))
              .catch((err) => res.send(err));
          } else {
            res.send('Email already exists!');
          }
        })
        .catch((err) => res.send(err));
    } else {
      res.send({ message: 'Email already exist' });
    }
  });
};

//TODO: Finish comparison and return generated token
// const loginUser = (req, res) => {
//   const { email, password } = req.body;

//   User.findOne({ email })
//     .then((result) => {
//       let hashPassword = bcrypt.compareSync(password, result.password);

//       if (hashPassword) {
//         return res.send({ accessToken: auth.generateToken(result) });
//       } else {
//         res.send('Incorrect credentials, please login');
//       }
//     })
//     .catch((err) => res.send(err));
// };
let getAllUsers = (req, res) => {
  User.find({})
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

let loginUser = (req, res) => {
  User.findOne({ email: req.body.email })
    .then((foundUser) => {
      if (foundUser === null) {
        return res.send('No user found in the database');
      } else {
        const isPasswordCorrect = bcrypt.compareSync(
          req.body.password,
          foundUser.password
        );

        if (isPasswordCorrect) {
          return res.send({ accessToken: auth.generateToken(foundUser) });
        } else {
          return res.send('Incorrect password, please try agan');
        }
      }
    })
    .catch((err) => res.send(err));
};

let upateAdmin = (req, res) => {
  // console.log(req.user.id); // id of the logged in user

  // console.log(req.params.id); //id of the user we want to update

  let updates = {
    isAdmin: true,
  };

  User.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((updateUser) => res.send(updateUser))
    .catch((err) => res.send(err));
};

let orderItem = async (req, res) => {
  /* 
    1. Find user by their ID (check if they're logged in)
          Once found --- push the details of the product that we're trying to push to a new subdocument in our user
    2. Be able to find product by their id
          once found - push the details of the user that's trying to place an order
    3. Calculate the sum of all the product ordered
  */
  //validate if the buyer isn't an admin
  if (req.user.isAdmin) {
    return res.send('Admin is not permitted to place an order');
  }

  let findUserById = await User.findById(req.user.id)
    .then((user) => {
      // console.log(user);

      let product = {
        productId: req.body.productId,
      };

      user.orders.push(product);

      return user
        .save()
        .then((result) => true)
        .catch((err) => err.message);
    })
    .catch((err) => res.send(err));

  if (!findUserById) {
    return res.send({ message: findUserById });
  }

  let findProductById = await Product.findById(req.body.productId).then(
    (product) => {
      let user = {
        userId: req.user.id,
      };

      product.orderPlacer.push(user);

      return product
        .save()
        .then((result) => true)
        .catch((err) => err.message);
    }
  );

  if (!findProductById) {
    return res.send({ message: findProductById });
  }

  let findAllProductsOrdered = await User.findById(req.user.id).then(
    (results) => {
      console.log(results);
      results.orders.forEach((product) => {
        Product.findById(product.productId)
          .then((item) => {
            for (let i = 0; i < results.orders.length; i++) {
              results.orders[i].totalAmount.push(item.price);
            }
          })
          .catch((err) => err.message);
      });

      return results
        .save()
        .then((send) => send)
        .catch((err) => err.message);
    }
  );

  if (findUserById && findProductById) {
    let sum = findAllProductsOrdered.orders[0].totalAmount.reduce(
      (prev, current) => prev + current,
      0
    );

    let newSum = {
      totalSum: sum,
    };

    await User.findByIdAndUpdate(req.user.id, newSum, { new: true })
      .then((updateUser) => updateUser)
      .catch((err) => err.message);

    return res.send({
      message: 'Successfully placed an order',
      totalAmount: sum,
    });
  }
};

// view single user's orders

// let getUserOrders = (req, res) => {
//   User.findById(req.user.id)
//     .then((result) => res.send(result))
//     .catch((err) => res.send(err));
// };

let getUserOrders = (req, res) => {
  User.findById(req.user.id)
    .then(async (result) => {
      console.log(typeof result);
      let modifiedProducts = [];

      modifiedProducts.push({
        _id: result._id,
        email: result.email,
        isAdmin: result.isAdmin,
        isActive: result.isActive,
        orders: [],
        orderTotal: result.totalSum,
      });
      //https://stackoverflow.com/questions/70806104/cant-push-object-into-the-array-after-running-mongodb-query
      for (const itemResult of result.orders) {
        await Product.findById(itemResult.productId).then((item) => {
          modifiedProducts[0].orders.push({
            id: item._id,
            itemName: item.name,
            categories: item.categories,
            price: item.price,
          });
        });
      }

      res.send(modifiedProducts);
    })
    .catch((err) => res.send(err));
};

const deleteUser = (req, res) => {
  User.findByIdAndDelete(req.params.id, (err, docs) => {
    err ? console.log(err) : res.send(`Deleted ${docs}`);
  });
};

const setUserAsAdmin = (req, res) => {
  let updates = {
    isAdmin: true,
  };

  User.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((updateUser) => res.send(updateUser))
    .catch((err) => res.send(err));
};

module.exports = {
  registerUser,
  loginUser,
  upateAdmin,
  getAllUsers,
  orderItem,
  getUserOrders,
  deleteUser,
  setUserAsAdmin,
};
