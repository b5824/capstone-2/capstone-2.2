const Product = require('../models/Product');

const createProduct = (req, res) => {
  const { name, categories, description, price } = req.body;

  //TODO: Check if object is a duplicate, e.g. name, categories, description and price are all the same
  const newProduct = new Product({
    name,
    categories,
    description,
    price,
  });

  newProduct
    .save()
    .then((result) => res.send(result))
    .catch((err) =>
      res.send({
        message: err.message,
        choices: err.errors.categories.properties.enumValues,
      })
    );
};

const findAllProduct = (req, res) => {
  Product.find({})
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

const findAllActiveProduct = (req, res) => {
  Product.find({ isActive: true })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

const findProductByID = (req, res) => {
  Product.findOne({ _id: req.params.id })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

const findProductByName = (req, res) => {
  Product.find({ name: req.body.name })
    .then((result) => res.send(result))
    .catch((err) => res.send(err));
};

/* 
let updateCourse = (req, res) => {
  let updates = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  };

  Course.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((upCourse) => res.send(upCourse))
    .catch((error) => res.send(error));
};
*/
const editProduct = (req, res) => {
  let updateItem = {
    name: req.body.name,
    categories: req.body.categories,
    description: req.body.description,
    price: req.body.price,
  };

  Product.findByIdAndUpdate(req.params.id, updateItem, { new: true })
    .then((result) => res.send(result))
    .catch((error) => res.send(error));
};

//Retrieve ALL orders, admin only
let retrieveAllOrders = (req, res) => {
  let arr = [];
  Product.find({})
    .then((result) => {
      result.forEach((item) => {
        item.orderPlacer.length !== 0 &&
          arr.push({
            name: item.name,
            categories: item.categories,
            description: item.description,
            price: item.price,
            orderPlacer: item.orderPlacer,
          });
      });
      arr.length === 0
        ? res.send({ message: 'No order has been placed yet' })
        : res.send({ arr });
    })
    .catch((err) => res.send(err));
};

//archive a product
let archiveAProduct = (req, res) => {
  Product.findById(req.body.id).then(async (product) => {
    product.isActive
      ? await Product.findByIdAndUpdate(
          req.body.id,
          { isActive: false },
          { new: true }
        ).then((result) => res.send(result))
      : await Product.findByIdAndUpdate(req.body.id, { isActive: true }).then(
          (result) => res.send(result)
        );
  });
};
let activateArchivedAProduct = (req, res) => {
  Product.findById(req.body.id).then(async (product) => {
    !product.isActive
      ? await Product.findByIdAndUpdate(req.body.id, { isActive: true }).then(
          (result) => res.send(result)
        )
      : await Product.findByIdAndUpdate(
          req.body.id,
          { isActive: true },
          { new: true }
        ).then((result) => res.send(result));
  });
};

const deleteProduct = (req, res) => {
  Product.findByIdAndDelete(req.params.id, (err, docs) => {
    err ? console.log(err) : res.send(`Deleted ${docs}`);
  });
};

module.exports = {
  createProduct,
  findAllProduct,
  findProductByID,
  findProductByName,
  editProduct,
  retrieveAllOrders,
  archiveAProduct,
  deleteProduct,
  findAllActiveProduct,
  activateArchivedAProduct,
};
